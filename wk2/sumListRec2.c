// Sum List

#include <stdio.h>
#include <stdlib.h>

typedef struct _node {
   int  value;
   struct _node *next;
} Node;

void sumList(Node *L, int *sum);

int main(int argc, char **argv) {

    Node *L;
    L = malloc(sizeof(Node));
    L->value = 5;
    L->next = malloc(sizeof(Node));
    L->next->value = 1;
    L->next->next = malloc(sizeof(Node));
    L->next->next->value = 4;
    L->next->next->next = NULL;

    int sum = 0;
    sumList(L, &sum);

    printf("Sum: %d\n", sum);

    return EXIT_SUCCESS;
}

void sumList(Node *L, int *sum) {
    if (L != NULL) {
        *sum += L->value;
        sumList(L->next, sum);
    }
}
