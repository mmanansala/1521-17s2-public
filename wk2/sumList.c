// Sum List

#include <stdio.h>
#include <stdlib.h>

typedef struct _node {
   int  value;
   struct _node *next;
} Node;

int sumList(Node *L);

int main(int argc, char **argv) {

    Node *L;
    L = malloc(sizeof(Node));
    L->value = 5;
    L->next = malloc(sizeof(Node));
    L->next->value = 1;
    L->next->next = malloc(sizeof(Node));
    L->next->next->value = 4;
    L->next->next->next = NULL;

    printf("Sum: %d\n", sumList(L));

    return EXIT_SUCCESS;
}

int sumList(Node *L) {

    return 0;
}