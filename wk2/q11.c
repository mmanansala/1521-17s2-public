#include <stdio.h>
#include <stdlib.h>

typedef unsigned int Word;

Word reverseBits(Word w);

int main(int argc, char **argv) {

    if (argc > 1) {
        Word n = strtol(argv[1], NULL, 16);
        printf("0x%X\n", n);
        printf("0x%X\n", reverseBits(n));
    }

    return EXIT_SUCCESS;
}

Word reverseBits(Word w) {
    return 0;
}