#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    unsigned short a, b, c;
    a = 0x5555;  b = 0xAAAA;  c = 0x0001;

    printf("a | b = 0x%X\n", a | b);
    printf("a & b = 0x%X\n", a & b);
    printf("a ^ b = 0x%X\n", a ^ b);
    printf("a & ~b = 0x%X\n", a & ~b);
    printf("c << 6 = 0x%X\n", c << 6);
    printf("a >> 4 = 0x%X\n", a >> 4);
    printf("a & (b << 1) = 0x%X\n", a & (b << 1));
    printf("b | c = 0x%X\n", b | c);
    printf("a & ~c = 0x%X\n", a & ~c);

    return EXIT_SUCCESS;
}