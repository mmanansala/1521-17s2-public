#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    int nums[] = {1, 8, 10, 15, 16, 100, 127, 200};

    int i;
    for (i = 0; i < 8; i++) {
        // Convert to binary
        char binary[9];
        int j;
        for (j = 0; j < 8; j++) {
            // See if bit is 1 or not
            unsigned char mask = (unsigned) 1 << (7 - j); 
            if (nums[i] & mask) {
               binary[j] = '1';
            } else {
               binary[j] = '0';
            }
        }
        binary[j] = '\0';
        printf("%d = %s = %o = 0x%X\n", nums[i], binary, nums[i], nums[i]);
    }

    return EXIT_SUCCESS;
}
