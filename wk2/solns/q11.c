#include <stdio.h>
#include <stdlib.h>

typedef unsigned int Word;

Word reverseBits(Word w);

int main(int argc, char **argv) {

    if (argc > 1) {
        Word n = strtol(argv[1], NULL, 16);
        printf("0x%X\n", n);
        printf("0x%X\n", reverseBits(n));
    }

    return EXIT_SUCCESS;
}

Word reverseBits(Word w) {
    Word mask1 = ((Word) 1) << 31;
    Word mask2 = 1;
    Word rev = 0;
    int i;
    for (i = 0; i < sizeof(Word) * 8; i++) {
        if (w & mask1) {
            rev |= mask2;
        }
        mask1 = mask1 >> 1;
        mask2 = mask2 << 1;
    }
    return rev;
}