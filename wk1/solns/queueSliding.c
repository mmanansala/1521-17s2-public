/*
Implementation of FIFO queue ADT
Uses a sliding array
Michael Manansala
*/

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

#define MAXQ 6
 
struct QueueRep {
   int nitems;
   int head;
   int tail;
   int items[MAXQ];
};

// create a new empty Queue
Queue makeQueue() {
   // You would think you could pass in sizeof(Queue) but that's just a pointer
   // You really need to pass in sizeof(struct QueueRep)
   Queue newQueue = malloc(sizeof(struct QueueRep));
   if (newQueue != NULL) { // Ensure newQueue is not NULL
      newQueue->nitems = 0;
      newQueue->head = 0;
      newQueue->tail = -1; // For the sake of niceness, 
      // tail should be head + nitems - 1
      // Don't actually need to initialize everything to 0
      // Just ignore anything beyond head and tail
      /*
      int i;
      for (i = 0; i < MAXQ; i++) {
         items[i] = 0;
      }
      */
   }
   return newQueue;
}

// delete memory associated with a Queue
void  freeQueue(Queue q) {
   if (q != NULL) {
      free(q);
   }
}

// insert a new item at the tail of the Queue
void  enterQueue(Queue q, int n) {
   if (q != NULL) {
      if (q->nitems < MAXQ) { // Ensure we don't overfill array
         q->items[++q->tail] = n;
         q->nitems++;
      }
   }
}

// remove/return the item at the head of the Queue
int leaveQueue(Queue q) {
   if (q != NULL) {
      if (q->nitems > 0) { // Ensure we actually have something
         q->nitems--;
         int ret = q->items[q->head];
         int i;
         // Move everything back
         for (i = 0; i < q->nitems; i++) {
            q->items[i] = q->items[i + 1];
         }
         q->tail--;
         return ret;
      }
   }
   return 0;
}

// return the number of items currently in the Queue
int lengthQueue(Queue q) {
   if (q != NULL) {
      return q->nitems;
   }
   return 0;
}