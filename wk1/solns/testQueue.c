/*
Program to test queue
Michael Manansala
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "queue.h"

int main(int argc, char **argv) {
   
   int n;
   if (argc > 1) {
      n = atoi(argv[1]);
      if (n > 6) {
         n = 6;
      }
   } else {
      n = 3; // Default value of 3
   }
   
   Queue q = makeQueue();
   assert(q != NULL);
   printf("Queue successfuly created\n");

   int i;
   for (i = 0; i < n; i++) {
      printf("Adding %d to queue\n", i);
      assert(lengthQueue(q) == i);
      enterQueue(q, i);
   }

   assert(lengthQueue(q) == n);

   printf("Now removing itesms from queue\n");
   for (i = 0; i < n; i++) {
      assert(leaveQueue(q) == i);
      assert(lengthQueue(q) == (n - i - 1));
   }

   assert(lengthQueue(q) == 0);

   for (i = 0; i < n; i++) {
      printf("Adding %d to queue\n", i);
      assert(lengthQueue(q) == i);
      enterQueue(q, i);
   }

   assert(lengthQueue(q) == n);

   printf("Now removing itesms from queue\n");
   for (i = 0; i < n; i++) {
      assert(leaveQueue(q) == i);
      assert(lengthQueue(q) == (n - i - 1));
   }

   assert(lengthQueue(q) == 0);

   freeQueue(q);

   printf("Successfully passed all tests\n");

   return 0;

}