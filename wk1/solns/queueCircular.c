/*
Implementation of FIFO queue ADT
Uses a circular array
Michael Manansala
*/

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

#define MAXQ 6
 
struct QueueRep {
   int nitems;
   int head;
   int tail;
   int items[MAXQ];
};

// create a new empty Queue
Queue makeQueue() {
   // You would think you could pass in sizeof(Queue) but that's just a pointer
   // You really need to pass in sizeof(struct QueueRep)
   Queue newQueue = malloc(sizeof(struct QueueRep));
   if (newQueue != NULL) { // Ensure newQueue is not NULL
      newQueue->nitems = 0;
      newQueue->head = 0;
      newQueue->tail = MAXQ - 1; // For the sake of niceness, 
      // tail should be (head + nitems - 1) % MAXQ
      // Don't actually need to initialize everything to 0
      // Just ignore anything beyond head and tail
      /*
      int i;
      for (i = 0; i < MAXQ; i++) {
         items[i] = 0;
      }
      */
   }
   return newQueue;
}

// delete memory associated with a Queue
void  freeQueue(Queue q) {
   if (q != NULL) {
      free(q);
   }
}

// insert a new item at the tail of the Queue
void  enterQueue(Queue q, int n) {
   if (q != NULL) {
      if (q->nitems < MAXQ) { // Ensure we don't overfill array
         q->items[(++q->tail) % MAXQ] = n;
         q->nitems++;
      }
   }
}

// remove/return the item at the head of the Queue
int leaveQueue(Queue q) {
   if (q != NULL) {
      if (q->nitems > 0) { // Ensure we actually have something
         q->nitems--;
         int ret = q->items[q->head++];
         q->head = q->head % MAXQ; // Loop back around if necessary
         return ret;
      }
   }
   return 0;
}

// return the number of items currently in the Queue
int lengthQueue(Queue q) {
   if (q != NULL) {
      return q->nitems;
   }
   return 0;
}