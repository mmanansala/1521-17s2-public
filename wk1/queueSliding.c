/*
Implementation of FIFO queue ADT
Uses a circular array
Michael Manansala
*/

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

#define MAXQ 6
 
struct QueueRep {
   int nitems;
   int head;
   int tail;
   int items[MAXQ];
};

// create a new empty Queue
Queue makeQueue();

// delete memory associated with a Queue
void  freeQueue(Queue);

// insert a new item at the tail of the Queue
void  enterQueue(Queue, int);

// remove/return the item at the head of the Queue
int   leaveQueue(Queue);

// return the number of items currently in the Queue
int   lengthQueue(Queue);