#include <stdio.h>
 
char *s1 = "abc";
 
int main(void)
{  
   char *s2 = "def";
   
   printf("Location of s1: %p\n", &s1);
   printf("Contents of s1: %p\n", s1);

   printf("Location of s2: %p\n", &s2);
   printf("Contents of s2: %p\n", s2);

   return 0;
}